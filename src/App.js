import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import Header from './Header';
import Post from './Post';

import './style.scss';

const path = require('path');

class App extends Component {
  state = {
    posts: [
      {
        autor: 'Jose Claudio',
        avatar: path.join(__dirname, 'jose.png'),
        tempo: 'há 3 min',
        texto:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porttitor mauris quis pretium consectetur. Morbi et lacus quis lacus hendrerit mattis. Vestibulum vel ornare quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam in vulputate ligula. Maecenas mollis mauris sed nunc bibendum ullamcorper. Ut porttitor metus elementum quam ultricies sagittis. Morbi luctus consectetur neque, vel consequat nisi congue at. Suspendisse rhoncus risus vel sem maximus vestibulum. Quisque ex libero, dignissim at volutpat a, interdum sit amet purus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In nisi turpis, sollicitudin id eleifend sed, cursus id nisi. Praesent sagittis orci ligula, a congue velit hendrerit sollicitudin. Vivamus at condimentum nisl, gravida porttitor nibh. Suspendisse accumsan blandit metus eget dapibus. Morbi dictum ut lacus eu placerat.',
      },
      {
        autor: 'Jercica Patricia',
        avatar: path.join(__dirname, 'jercica.jpg'),
        tempo: 'há 20 min',
        texto:
          'Suspendisse bibendum scelerisque sodales. Suspendisse potenti. Suspendisse potenti. Vivamus vulputate at lectus sed lobortis. Duis dolor nisi, lobortis ac euismod at, varius sit amet elit. Duis tincidunt tempor lectus in pulvinar. Etiam luctus ipsum odio, sed scelerisque tortor commodo vitae. Nulla eu leo eget elit aliquam condimentum vitae id ex. Etiam libero tellus, ornare at ornare ut, porta nec felis. Pellentesque ullamcorper ullamcorper eros, nec dictum massa volutpat sit amet. Fusce aliquam efficitur ex id rutrum.',
      },
      {
        autor: 'Gilma Freitas',
        avatar: path.join(__dirname, 'gilma.jpg'),
        tempo: 'há 2 dias',
        texto:
          'Nam porttitor mauris quis pretium consectetur. Morbi et lacus quis lacus hendrerit mattis. Vestibulum vel ornare quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam in vulputate ligula. Maecenas mollis mauris sed nunc bibendum ullamcorper. Ut porttitor metus elementum quam ultricies sagittis. Morbi luctus consectetur neque, vel consequat nisi congue at. Suspendisse rhoncus risus vel sem maximus vestibulum. Quisque ex libero, dignissim at volutpat a, interdum sit amet purus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In nisi turpis, sollicitudin id eleifend sed, cursus id nisi. Praesent sagittis orci ligula, a congue velit hendrerit sollicitudin.',
      },
    ],
  };

  render() {
    const { posts } = this.state;

    return (
      <Fragment>
        <Header />

        {posts.map((post, _i) => <Post data={post} />)}
      </Fragment>
    );
  }
}

render(<App />, document.getElementById('app'));
