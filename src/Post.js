import React, { Fragment } from 'react';

import PostHeader from './PostHeader';

const Post = props => (
  <Fragment>
    <div className="post">
      <PostHeader avatar={props.data.avatar} autor={props.data.autor} tempo={props.data.tempo} />
      <hr />
      <p className="conteudo">
        {props.data.texto}
      </p>
    </div>
  </Fragment>
);

export default Post;
