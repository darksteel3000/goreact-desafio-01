import React, { Fragment } from 'react';

const PostHeader = props => (
  <Fragment>
    <img src={props.avatar} />
    <p className="nome">
      {props.autor}
    </p>
    <p className="tempo">
      {props.tempo}
    </p>
  </Fragment>
);

export default PostHeader;
